# ForEmias

## Description
Тестовое задание, сделанное для проекта ЕМИАС

## Usage

#### For local usage
pytest -m dev --alluredir results -vv

#### For remote usage
pytest -m dev --alluredir results -vv --remote --ip_address "Your TCP connection"
