import allure
import pytest
from _pytest.fixtures import Parser, Config

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


def pytest_addoption(parser: Parser):

    # Сгруппированные параметры запуска драйвера управления браузером
    group_browser = parser.getgroup("option-browser", "Параметры запуска браузера")
    group_browser.addoption('--remote',
                            dest='remote',
                            action='store_true',
                            help='Использовать локальную версию драйвера или удаленную')
    group_browser.addoption('--ip_address',
                            dest='ip_address',
                            type=str,
                            default="http://127.0.0.1:4444",
                            help='Адресс для коннекта по удаленному доступу')


def pytest_configure(config: Config):
    # Добавляем необходимые нам маркеры

    config.addinivalue_line("markers", "dev: this mark test for development")


@pytest.fixture(scope='session', autouse=True)
def browser(pytestconfig):
    """Возвращает объект WebDriver."""

    if pytestconfig.getoption("remote"):
        with allure.step("Инициализация WebDriver для работы через TCP"):
            browser = webdriver.Remote(command_executor=pytestconfig.getoption("ip_address"))
    else:
        with allure.step("Инициализация WebDriver для работы на локальном компьютере"):
            browser = webdriver.Chrome(ChromeDriverManager().install())
    with allure.step("Посылаем команду браузеру - развернуть на полный экран"):
        browser.maximize_window()
    browser.implicitly_wait(5)
    yield browser
    with allure.step("Закрытие браузера"):
        browser.quit()
