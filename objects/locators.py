# -*- coding: utf-8 -*-
"""
Базовый класс расширенных локаторов

:author: Sharaev V.A
:maintainer: Sharaev V.A
:email: sharvladrus@gmail.com
:status: Development
"""

from selenium.webdriver.common.by import By
from dataclasses import dataclass, field
from typing import Tuple


@dataclass
class PageExtendLocator:
    """Базовый класс расширенных локаторов
    :param str by: Тип локатора
    :param str loc: Локатор
    :param str eng: Контекст на английском
    :param str rus: Контекст на русском
    :param tuple elm_loc: Кортеж
    :return: obj
    """

    by: str = None
    loc: str = None
    kind: str = 'ext_locator'
    name: str = 'extended locator'
    eng: str = None
    rus: str = None
    _by: str = field(default=None, init=False, repr=False)
    _loc: str = field(default=None, init=False, repr=False)
    elm_loc: Tuple = field(default=(), init=False, repr=False)

    @property
    def by(self) -> str:
        return self._by

    @by.setter
    def by(self, value: str):
        self._by = value
        self.elm_loc = (value, self.loc)

    @property
    def loc(self) -> str:
        return self._loc

    @loc.setter
    def loc(self, value: str):
        self._loc = value
        self.elm_loc = (self.by, value)

    def __post_init__(self):
        if type(self._by) is not str:
            self._by = By.CSS_SELECTOR
        if type(self._loc) is not str:
            self._loc = 'body'
        self.elm_loc = (self.by, self.loc)


class YandexMainPage(PageExtendLocator):

    SEARCH_FIELD = PageExtendLocator(loc=".input__control.mini-suggest__input", name='Поле поиска')
    PICTURES_BUTTON = PageExtendLocator(loc=".services-new__icon.services-new__icon_images", name="Кнопка картинки")
    LAMBDA_PICTURES_LIST = lambda num: PageExtendLocator(loc=f".PopularRequestList-Item_pos_{num} .PopularRequestList-SearchText",
                                                         name="Список с категориями картинок")
