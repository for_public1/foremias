import random
import time

import allure
import pytest

from objects.general import GeneralPage
from objects.locators import YandexMainPage as YMP


@allure.feature("Тестовое задание для selenium-разработчика.")
@allure.severity('critical')
@allure.story("Тест-кейс №1")
@pytest.mark.dev
def test_first_scenario(browser):

    executor = GeneralPage(browser)

    with allure.step('Переход на портал Яндеса, главная страница.'):
        executor.open_url("https://yandex.ru/")
        assert browser.title == "Яндекс", "Открыт не корректный урл."

    with allure.step('Проверка наличия поля "поиска".'):
        executor.check_element_displayed(YMP.SEARCH_FIELD)

    with allure.step('Проверка наличия кнопки "Картинки"'):
        executor.check_element_displayed(YMP.PICTURES_BUTTON)

    with allure.step('Переход в раздел "Картинки"'):
        executor.fool_click(YMP.PICTURES_BUTTON)

    with allure.step('Переключение на новое открывшееся окно браузера.'):
        executor.change_window(1)

    with allure.step('Проверка корректность урла после нажатия на кнопку Картинки.'):

        assert executor.get_current_url().startswith("https://yandex.ru/images/"), "Открыт не правильный урл."

    with allure.step('Клик по любой (из 10) категории картинок.'):

        random_int = random.randint(0, 10)
        chosen_category = executor.get_element_text(YMP.LAMBDA_PICTURES_LIST(random_int))
        executor.fool_click(YMP.LAMBDA_PICTURES_LIST(random_int))

    with allure.step('Проверка соответствия текста в поле поиска выбранной категории.'):
        assert executor.get_page_element(YMP.SEARCH_FIELD).get_attribute("value") == chosen_category, \
            "Текст в поле поиска должен совпадать с выбранной категорией!"

        time.sleep(4)

