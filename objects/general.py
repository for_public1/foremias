# -*- coding: utf-8 -*-
import datetime

from allure_commons.types import AttachmentType
from selenium.webdriver import ActionChains
import allure
from typing import Tuple
from time import sleep, time
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from objects.locators import PageExtendLocator


def wait_for(condition_function) -> bool:
    """ Метод ожидания возвращения True от аргумента.

    :param condition_function: принимает на вход функцию.
    :return: Boole
    """

    start_time = time()
    while time() < start_time + 5:
        if condition_function():
            return True
        else:
            sleep(0.1)
    raise Exception(
        f'Timeout waiting for {condition_function.__name__}')


class BasePage:

    def __init__(self, browser, timeout=15):
        """ Метод инициализации.

        :param browser: fixture инициализирующая браузер.
        :param timeout: int: времмя ожидания.
        """

        self.browser = browser
        self.browser.implicitly_wait(timeout)

    def scroll_to_element(self, element):
        actions = ActionChains(self.browser)
        actions.move_to_element(element).perform()

    def fool_wait(self, num: int):
        with allure.step(f"!!!Ослиное ожидание в {num} секунд!!!"):
            tik = 0
            while tik < num:
                tik += 0.1
                sleep(0.1)

    def wait_change_url(self, before_url, after_url, ):

        def change_url():
            return ec.url_changes(before_url)
        wait_for(change_url)

        def new_url():
            return ec.url_to_be(after_url)
        wait_for(new_url)

        def page_has_loaded():
            page_state = self.browser.execute_script(
                'return document.readyState;'
            )
            return page_state == 'complete'

        wait_for(page_has_loaded)

    def open_url(self, page_url):
        """ Метод открытия ссылки в браузере.

        :param page_url: Str: Url
        :return:
        """

        try:
            before_url = self.browser.current_url

            if before_url == page_url:
                self.browser.get('about:blank')
                before_url = 'about:blank'
                sleep(2)
            self.browser.get(page_url)
            self.wait_change_url(before_url, page_url)

        except:
            return False

    def open_next_page(self):
        """ Метод перехода вперёд в истории перемещения на странице.

        :return: boole
        """

        try:
            open_page = self.browser.current_url
            self.browser.forward()
            next_page = self.browser.current_url
            if open_page == next_page:
                sleep(0.1)
                self.browser.forward()

                def page_has_loaded():
                    page_state = self.browser.execute_script(
                        'return document.readyState;'
                    )
                    return page_state == 'complete'

                wait_for(page_has_loaded)
                sleep(0.4)
        except:
            return False

    def open_previous_page(self):
        """ Метод перехода назад в истории перемещения на странице.

        :return: boole
        """

        try:
            open_page = self.browser.current_url
            self.browser.back()
            before_page = self.browser.current_url
            if before_page == open_page:
                sleep(0.1)
                self.browser.back()

                def page_has_loaded():
                    page_state = self.browser.execute_script(
                        'return document.readyState;'
                    )
                    return page_state == 'complete'

                wait_for(page_has_loaded)
                sleep(0.4)
        except:
            return False

    def refresh_current_page(self):
        """ Метод обновления страницы.

        :return: Boole
        """

        try:
            open_page = self.browser.current_url
            self.browser.refresh()
            refreshed_page = self.browser.current_url
            if open_page != refreshed_page:
                sleep(0.1)
                self.browser.refresh()

                def page_has_loaded():
                    page_state = self.browser.execute_script(
                        'return document.readyState;'
                    )
                    return page_state == 'complete'

                wait_for(page_has_loaded)
                sleep(0.4)
            return True
        except:
            return False

    def change_window(self, num):
        """ Метод переход из одного окна браузера в другое.

        :param num: Int: номер окна, на которое нужно переключиться
        :return: Tuple [Boole and Str]
        """

        try:
            work_window = self.browser.window_handles[0]
            sleep(0.5)
            opened_window = self.browser.window_handles[num]
            move = self.browser.switch_to.window(opened_window)
            if opened_window == False:
                return False, "Не получилось переключить окно браузера!!!"
        except Exception:
            assert False, f"Переключение с одного окна {work_window} на другое {opened_window} не работает!!"


class GeneralPage(BasePage):

    def get_page_element(self, elm: PageExtendLocator, times: int = 15) -> Tuple:
        """ Метод получения элемент по локатору

        :param elm: PageExtendLocator
        :param times: Int: время ожидания.
        :return: Tuple [Boole and Str]
        """

        with allure.step(f'Поиск элемента по локатору {elm.elm_loc}'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            try:
                wd_wait = WebDriverWait(self.browser, times)
                element = wd_wait.until(ec.presence_of_element_located(elm.elm_loc),
                                        message=f"Нет элемента с локатором: {elm.elm_loc};\r\n Описание: {elm.name}")
                return element
            except Exception as e:
                assert False, f"""Поиск элемента по локатору {elm.elm_loc}. Исключение: {e}"""

    def get_page_elements(self, elm: PageExtendLocator, times: int = 15) -> Tuple:
        """ Метод массового получения элементов по локатору

        :param elm: PageExtendLocator
        :param times: Int: время ожидания.
        :return: Tuple [Boole and Str]
        """

        with allure.step(f'Поиск элемента по локатору {elm.elm_loc}'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            try:
                wd_wait = WebDriverWait(self.browser, times)
                elms = wd_wait.until(ec.presence_of_all_elements_located(elm.elm_loc),
                                     message=f"Нет элемента с локатором: {elm.elm_loc};\r\n Описание: {elm.name}")
                return elms
            except Exception as e:
                assert False, f"""Поиск элемента по локатору {elm.elm_loc}. Исключение: {e}"""

    def check_element_displayed(self, elm: PageExtendLocator) -> Tuple[bool, str]:
        """ Метод проверки отображения элемента на странице.

        :param elm: PageExtendLocator
        :param name:
        :return: Tuple [Boole and Str]
        """

        with allure.step(f'Проверка элемента "{elm.name}" с локатором {elm.elm_loc}'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            try:
                element = self.get_page_element(elm)
                state_displayed = element.is_displayed()
                self.highlight(element)
                msg = f'Проверка отображения элемента "{elm.name} с локатором {elm.elm_loc} \r\n"'
                return state_displayed, msg
            except Exception as e:
                assert False, f"""Метод не отработал {elm.elm_loc}. Исключение: {e}"""

    def highlight(self, element):
        """ Метод подсвечивания элементов на странице при проверке.

        :param element: принимает на вход функцию или локатор.
        :return: Boole
        """

        def apply_style(s):
            self.browser.execute_script("arguments[0].setAttribute('style', arguments[1]);", element, s)

        original_style = element.get_attribute('style')
        apply_style("border: 0.5px solid red;")
        sleep(.1)
        apply_style(original_style)
        sleep(.1)

    def fool_click(self, elm: PageExtendLocator) -> Tuple[bool, str]:
        """ Метод клика по элементу.

        :param elm: PageExtendLocator
        :return: Tuple [Boole and Str]
        """

        with allure.step(f'Клик по локатору {elm.elm_loc}'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            try:
                self.is_element_clickable(elm)
                self.get_page_element(elm).click()

                def page_has_loaded():
                    page_state = self.browser.execute_script(
                        'return document.readyState;'
                    )
                    return page_state == 'complete'

                wait_for(page_has_loaded)
                return True, "It's good!"
            except NoSuchElementException as e:
                assert False, f'Нет такого объкета {elm.name}! Исключение: {e}!'

    def get_current_url(self) -> str:
        """ Метод получения текущего Url-a.

        :return: str
        """

        with allure.step(f'Получение адреса страницы.'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            sleep(0.2)
            url = self.browser.current_url
            return url

    def get_element_text(self, elm: PageExtendLocator):
        """ Метод получения текст из тега.

        :param elm: PageExtendLocator
        :return: Str
        """

        with allure.step(f'Получение текста внутри отрк./закр. тега с параметром {elm.elm_loc}'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            try:
                element = self.get_page_element(elm)
                return str(element.text)
            except NoSuchElementException as e:
                assert False, f'Нет такого объкета {elm.name}! Исключение: {e}'

    def is_element_clickable(self, elm: PageExtendLocator, timeout: int = 10):
        """ Метод проверки кликабелности элемента.

        :param elm: PageExtendLocator
        :param timeout: Int: время ожидания
        :return: Tuple [Boole and Str]
        """

        with allure.step(f'Проверка кликабельности элемента по локатору {elm.elm_loc}'):
            allure.attach(self.browser.get_screenshot_as_png(), name=f'Screenshot_{datetime.datetime.now()}',
                          attachment_type=AttachmentType.PNG)
            try:
                WebDriverWait(self.browser, timeout).until(ec.element_to_be_clickable(elm.elm_loc))
            except TimeoutException:
                assert False, f'Не кликабельный эелемент {elm.name}!'
